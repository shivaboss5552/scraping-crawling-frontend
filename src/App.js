import React,{Fragment} from 'react';
import './App.css'
import Sidebar from './components/Sidebar/Sidebar.js'
import Header from './components/Header/Header.js'
import Analytics from './components/Analytics/Analytics.js'
import Testboard from './components/Testboard/Testboard.js'
import Main from './components/Main/Main.js'
import Dataset from './components/Dataset/Dataset'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


function App() {
  return (
    <Router>
      <Fragment>
        <Sidebar path={window.location.pathname} />

        <div className="main-container">
          <Header />
          <div className="sub-container">
            <Switch>
              <Route exact path="/">
                <Analytics />
              </Route>
              <Route exact path="/testboard">
                <Testboard />
              </Route>
              <Route exact path="/tools">
                <Main />
              </Route>

              <Route exact path="/dataset">
                <Dataset />
              </Route>

            </Switch>
          </div>
        </div>
      </Fragment>
    </Router>
  );
}

export default App;
