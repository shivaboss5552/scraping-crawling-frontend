import React,{useRef,useState,useEffect,Fragment} from 'react';
import ScraperIcon from '../../assets/Sidebar/scraper.png';
import CrawlerIcon from '../../assets/Sidebar/crawler.svg';
import LoadingIcon from '../../assets/Analytics/pulse.gif';
import './Main.css'
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';
import axios from 'axios';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import name from '../domain.js';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
}));

const useStylesExpand = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));


function Main(){
  const classes = useStyles();
  const classesExpand = useStylesExpand();
  const selectRef = useRef('');
  const [allWebsites,setallWebsites] = useState([]);
  const [alert,setalert] = useState({show:false,message:"",type:""});
  const [loading,setloading] = useState(false);
  const [current,setcurrent] = useState({type:"",content:{}});

  useEffect(function Loader(){

    async function getWebsites(){
      const websites = await axios.get(`http://${name}/api/websites/`).then(res => res.data.active)


      setallWebsites(websites.map(item => ({title:item})));
    }
    getWebsites()

  },[])

  async function scrapeORcrawl(e,type){
    setloading(true);
    setalert({show:false,message:"",type:""})
    const websiteName = selectRef.current.getElementsByTagName('input')[0].value

    if (websiteName === ''){
      setalert({show:false,message:"Website Name Cant be empty!",type:"error"});
      setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);
    }

    else{

      const response = await axios.get(`http://${name}/api/${type}/?name=${websiteName}`).then(res => res.data).catch(err => err.response.data)
      console.log(response)
      if (Object.keys(response).includes('error')){
        setalert({show:true,message:response.error,type:"error"});
        if (response.item){
          setcurrent({type:"scraping",content:{url:response.item}})
        }
        else{
          setcurrent({type:"",content:{}})
        }
      }
      else if (Object.keys(response).includes('id') && type === 'scrape'){
      setcurrent({type:"scraping",content:response})
      setTimeout(function(){console.log(current)},3000)
      setalert({show:true,message:"Scraped Successfully",type:"success"});
      setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);}

      else if (Object.keys(response).includes('url') && type === 'crawl'){
      console.log("went to crawling!")
      setcurrent({type:"crawling",content:response})
      setalert({show:true,message:"Crawled Successfully",type:"success"});
      setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);}

      else{
        setalert({show:true,message:'Something Went Wrong!',type:"error"});
        setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);
      }


    }

  setloading(false);
  }

  return <div className="mainContainer">
            <div className="mainBox">
              <div className="mainHeading">Scraping & Crawling Section</div>
              <div className="mainChoices">
                <div className="mainController">

                  <Autocomplete
                    ref={selectRef}
                    id="combo-box-demo"
                    options={allWebsites}
                    getOptionLabel={(option) => option.title}
                    style={{ width: 300 }}
                    renderInput={(params) => <TextField {...params} label="Select a Website" variant="outlined" />}
                  />

                  <div className={classes.root}>
                     <ButtonGroup variant="contained" color="primary" aria-label="contained primary button group">
                       <Button onClick={(e) => scrapeORcrawl(e,"scrape")} ><img src={ScraperIcon} style={{width:"32px"}} alt=""/>&nbsp;&nbsp;<span>Scrape</span></Button>
                       <Button onClick={(e) => scrapeORcrawl(e,"crawl")} ><img src={CrawlerIcon}  style={{width:"32px"}} alt=""/>&nbsp;&nbsp;<span>Crawl</span></Button>
                     </ButtonGroup>
                  </div>

                  {loading && <img src={LoadingIcon} style={{width:"76px"}} alt=""/> }

                </div>

                <div>
                  <Alert  severity={alert.type}>{alert.message}</Alert>
                </div>

                { current.type !== "" && <div className="mainResults">

                  { current.type === "scraping" && <div className="scraperResults">
                    {Object.keys(current.content).map(key =>
                        <Fragment>
                            <ExpansionPanel defaultExpanded={true} >
                                <ExpansionPanelSummary
                                  expandIcon={<ExpandMoreIcon />}
                                  aria-controls="panel2a-content"
                                  id="panel2a-header"
                                >
                                  <Typography className={classesExpand.heading}><h3>{key}</h3></Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                  <Typography>
                                    {current.content[key] ? current.content[key].toString() : <span className="errorline">No Data Found!</span>}
                                  </Typography>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        </Fragment>

                    )}
                  </div> }

                  { current.type === "crawling" && <div className="crawlerResults">

                        <Fragment>
                          <ExpansionPanel defaultExpanded={true} >
                              <ExpansionPanelSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel2a-content"
                                id="panel2a-header"
                              >
                                <Typography className={classesExpand.heading}><h3>URL</h3></Typography>
                              </ExpansionPanelSummary>
                              <ExpansionPanelDetails>
                                <Typography>
                                  <a href={current.content.url}>{current.content.url}</a>
                                </Typography>
                              </ExpansionPanelDetails>
                          </ExpansionPanel>

                            <ExpansionPanel defaultExpanded={true} >
                                <ExpansionPanelSummary
                                  expandIcon={<ExpandMoreIcon />}
                                  aria-controls="panel2a-content"
                                  id="panel2a-header"
                                >
                                  <Typography className={classesExpand.heading}><h3>Interesing URLs</h3></Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                  <Typography>
                                  <div className="urlList">
                                    <ol>
                                      { current.content.interesting_urls.length === 0 && <span className="errorline">No Interesting Urls Found!</span>}
                                      {current.content.interesting_urls.map(item => <Fragment><li><a href={item}>{item}</a></li><br/></Fragment>)}
                                    </ol>
                                  </div>
                                  </Typography>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>

                            <ExpansionPanel defaultExpanded={true} >
                                <ExpansionPanelSummary
                                  expandIcon={<ExpandMoreIcon />}
                                  aria-controls="panel2a-content"
                                  id="panel2a-header"
                                >
                                  <Typography className={classesExpand.heading}><h3>Non Interesing URLs</h3></Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                  <Typography>
                                    <div className="urlList">
                                      <ol>
                                        { current.content.non_interesting.length === 0 && <span className="errorline">No Non Interesting Urls Found!</span>}
                                        {current.content.non_interesting.map(item => <Fragment><li><a href={item}>{item}</a></li><br/></Fragment>)}
                                      </ol>
                                    </div>
                                  </Typography>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>

                        </Fragment>


                  </div> }





                </div>}

              </div>
            </div>
         </div>
}

export default Main;
