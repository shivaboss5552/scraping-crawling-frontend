import React,{useState,useEffect} from 'react';
import './Controls.css';
import Button from '@material-ui/core/Button';
import EditIcon from '../../assets/Analytics/edit.svg';
import Scraper from '../../assets/Sidebar/scraper.png';
import Crawler from '../../assets/Sidebar/crawler.svg';
import axios from 'axios';

import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Input } from '@material-ui/core';

import name from '../domain.js';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

function Controls(props){

  const allWebsites = props.allWebsites
  const classes = useStyles();
  const [Edit,setEdit] = useState(false);

  const [statusData,setstatusData] = useState({
                                                "is_scraping": null,
                                                "is_crawling": null,
                                                "scraping": 0,
                                                "crawling": 0,
                                                "scraping_limit":0,
                                                "crawling_limit":0
                                            });

  

  const handleChange = (event) => {

    setstatusData({...statusData,[event.target.name]:event.target.value})
  };

  async function applyChanges (){

    console.log(statusData,"button clicked")

    const postData = {
      is_scraping:(statusData.scraping === 0 ? false : true),
      is_crawling:(statusData.crawling === 0 ? false : true),
      scraping:(statusData.scraping === 0 ? 'eventshigh' : statusData.scraping),
      crawling:(statusData.crawling === 0 ? 'eventshigh' : statusData.crawling),
      scraping_limit:(statusData.scraping_limit === "" ? 0 : parseInt(statusData.scraping_limit)),
      crawling_limit:(statusData.crawling_limit === "" ? 0 : parseInt(statusData.crawling_limit))

    }

    const response = await axios.post(`http://${name}/api/status/`,postData).then(res => res.data)

    setstatusData(response);
    setEdit(false);




  }

  useEffect(() => {
    async function getStatus(){
      const status = await axios.get(`http://${name}/api/status/`).then(res => res.data)
      setstatusData(status)
    }

    getStatus()
  },[])


  return <div className="controls">
    <span className="controls-heading">
      <span>Status </span>
      <Button onClick = {() => setEdit(true)} variant="contained" color="default"><img src={EditIcon} alt="Edit controls"/>&nbsp;&nbsp;Edit</Button>
    </span>

    {!Edit ? <div className="controls-content">
      <span className="controls-scraping">
        <span className="key">SCRAPING</span>
        <span className={"value "+(statusData.is_scraping ? "green" : "red" )}> {statusData.is_scraping? `ON (${statusData.scraping})`: 'OFF' }</span>
      </span>
      <span className="controls-crawling">
        <span className="key">CRAWLING</span>
        <span className={"value "+(statusData.is_crawling ? "green" : "red" )}> {statusData.is_crawling? `ON (${statusData.crawling})`: 'OFF' }</span>
      </span>
      <span className="controls-scraping-limit">
        <span className="key">
          <span>Limit&nbsp;</span>
          <img src={Scraper} width="20px" alt=""/></span>
        <span className="value">{statusData.scraping_limit ? `${statusData.scraping_limit}/day` :'NO LIMIT'}</span>
      </span>
      <span className="controls-crawling-limit">
        <span className="key">
          <span>Limit&nbsp;</span>
          <img src={Crawler} width="20px" alt=""/></span>
        <span className="value">{statusData.crawling_limit ? `${statusData.crawling_limit}/day` :'NO LIMIT'}</span>
      </span>
    </div>:
      <div className="controls-edit">
        <div className="controls-edit-sc">
          <img src={Scraper} width="20px" alt=""/>
            <FormControl className={classes.formControl}>

            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name= "scraping"
              value={statusData.scraping}
              onChange={handleChange}
            >
              <MenuItem value={0}>OFF</MenuItem>
              {allWebsites.map(item => <MenuItem value={item}>{item.toUpperCase()}</MenuItem>)}

            </Select>
          </FormControl>
      </div>
      <div className="controls-edit-sc">
          <img src={Crawler}  style={{width:"10px"}} alt=""/>
          <FormControl className={classes.formControl}>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="crawling"
              value={statusData.crawling}
              onChange={handleChange}
            >
              <MenuItem value={0}>OFF</MenuItem>
              {allWebsites.map(item => <MenuItem value={item}>{item.toUpperCase()}</MenuItem>)}

            </Select>
          </FormControl>
      </div>

     <Input name = "scraping_limit" value = {statusData.scraping_limit} onChange={handleChange} placeholder="Set a Daily Scraping Limit!"/>

     <Input name = "crawling_limit" value = {statusData.crawling_limit} onChange={handleChange} placeholder="Set a Daily Crawling Limit!"/>

     <div id="save">
       <Button variant="contained" onClick = {applyChanges} color="secondary">Apply!</Button>
     </div>

      </div>}
  </div>
}

export default Controls;
