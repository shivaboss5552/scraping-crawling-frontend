import React,{useEffect,useState} from 'react';
import {Doughnut} from 'react-chartjs-2';
import Success from '../../assets/Analytics/success.svg';
import Failure from '../../assets/Analytics/fail.svg';
import Remaining from '../../assets/Analytics/remaining.svg';
import Loader from '../Loader/Loader';
import Controls from './Controls.js';
import Dashboard from './Dashboard.js';
import axios from 'axios';
import './Analytics.css';
import name from '../domain.js';



function Analytics (){

  const [Total,setTotal] = useState({scraping_data:
                                          {total:0,
                                          passed:0,
                                          failed:0,
                                          remaining:0,
                                          "last":""}
                                    ,crawling_data:
                                          {total:0,
                                          passed:0,
                                          failed:0,
                                          remaining:0,
                                          "last":0},
                                    allWebsites:[],
                                    allWebsitesData:[]});
  const [Loading,setLoading] = useState(false);

  const {scraping_data,crawling_data,allWebsites,allWebsitesData} = Total;


  useEffect(function test(){
    console.log("App Loaded")
    async function fetchData(){
      setLoading(true);
     const data = await axios.get(`http://${name}/api/analytics/`).then(res => res.data)
     console.log(data)


     const processed = data.reduce((x,y) => {

       const dateConvert = (date) => {
         date = date.split("/")
         return date[1]+"/"+date[0]+"/"+date[2]
       }
       const dateCompare = (a,b) => {
         let date1 = new Date(dateConvert(a)).getTime()
         let date2 = new Date(dateConvert(b)).getTime()
         return (date1 > date2 ? a : b)
       }

          return {
            	scraping_data: {
            		total: x.scraping_data.total + y.scraping_data.total,
            		passed: x.scraping_data.passed + y.scraping_data.passed,
            		failed: x.scraping_data.failed + y.scraping_data.failed,
            		remaining: x.scraping_data.remaining + y.scraping_data.remaining,
            		"last": dateCompare(x.scraping_data.last,y.scraping_data.last)
            	},
            	crawling_data: {
                total: x.crawling_data.total + y.crawling_data.total,
            		passed: x.crawling_data.passed + y.crawling_data.passed,
            		failed: x.crawling_data.failed + y.crawling_data.failed,
            		remaining: x.crawling_data.remaining + y.crawling_data.remaining,
                "last": dateCompare(x.crawling_data.last,y.crawling_data.last)
            	}
            }


     })
    console.log(processed)

    setTotal({...processed,allWebsites:data.map(item => item.name),allWebsitesData:data})

    setLoading(false);
   }
   fetchData()

 },[])

  return (Loading ? <Loader/> : <div className="analytics-container">
          <div className="overview">
            <div className="sc">
              <span className="sc-heading">Scraping Details</span>
              <div className="sc-container">
                  <div className="sc-graph">
                    <Doughnut data={{
                  	labels: [
                  		'Passed',
                  		'Failed',
                  		'Remaining'
                  	],
                  	datasets: [{
                  		data: [scraping_data.passed, scraping_data.failed, scraping_data.remaining],
                  		backgroundColor: [
                  		'#33CC66',
                  		'#cc3300',
                  		'#FFCE56'
                  		],
                  		hoverBackgroundColor: [
                  		'#FF6384',
                  		'#36A2EB',
                  		'#FFCE56'
                  		]
                  	}]
                  }}  options={{ maintainAspectRatio: false }}/>
                  </div>

                  <div className="sc-info">
                      <span className="sc-total" style={{alignSelf:"flex-start"}}>
                        <span className = "focus" >Total Items :</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span className="values" >{scraping_data.total}</span>
                      </span>
                      <span className="sc-last" style={{alignSelf:"flex-start"}}>
                        <span className = "focus" >Last Scraped :</span>&nbsp;&nbsp;<span className="values" >{scraping_data.last}</span>
                      </span>
                        <span className = "sc-percentage-container">

                          <div>
                            <img src={Success} width = "34px" alt="" style = {{width:"2em"}}/>
                            <span style = {{color:"green",fontSize:"1.5em"}}>&nbsp;{((scraping_data.passed/scraping_data.total)*100).toFixed(0)}%</span>
                          </div>

                          <div>
                            <img src={Failure} width = "34px" alt="" style = {{width:"2em"}}/>
                            <span style = {{color:"red",fontSize:"1.5em"}} >&nbsp;{((scraping_data.failed/scraping_data.total)*100).toFixed(0)}%</span>
                          </div>

                          <div>
                            <img src={Remaining} width = "34px" alt="" style = {{width:"2em"}}/>
                            <span style = {{color:"grey",fontSize:"1.5em"}}>&nbsp;{((scraping_data.remaining/scraping_data.total)*100).toFixed(0)}%</span>
                          </div>
                        </span>

                  </div>
              </div>
            </div>


            <div className="sc">
              <span className="sc-heading">Crawling Details</span>
              <div className="sc-container">

                  <div className="sc-graph">
                    <Doughnut data={{
                    labels: [
                      'Passed',
                      'Failed',
                      'Remaining'
                    ],
                    datasets: [{
                      data: [crawling_data.passed, crawling_data.failed, crawling_data.remaining],
                      backgroundColor: [
                      '#33CC66',
                      '#cc3300',
                      '#FFCE56'
                      ],
                      hoverBackgroundColor: [
                      '#FF6384',
                      '#36A2EB',
                      '#FFCE56'
                      ]
                    }]
                    }}  options={{ maintainAspectRatio: false }}/>
                  </div>

                  <div className="sc-info">
                      <span className="sc-total" style={{alignSelf:"flex-start"}}>
                        <span className = "focus" >Total Items :</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span className="values">{crawling_data.total}</span>
                      </span>
                      <span className="sc-last" style={{alignSelf:"flex-start"}}>
                        <span className = "focus" >Last Crawled :</span>&nbsp;&nbsp;<span className="values" >{crawling_data.last}</span>
                      </span>
                        <span className = "sc-percentage-container">

                          <div>
                            <img src={Success} width = "34px" style = {{width:"2em"}} alt=""/>
                            <span style = {{color:"green",fontSize:"1.5em"}}>&nbsp;{((crawling_data.passed/crawling_data.total)*100).toFixed(0)}%</span>
                          </div>

                          <div>
                            <img src={Failure} width = "34px" alt="" style = {{width:"2em"}} />
                            <span style = {{color:"red",fontSize:"1.5em"}} >&nbsp;{((crawling_data.failed/crawling_data.total)*100).toFixed(0)}%</span>
                          </div>

                          <div>
                            <img src={Remaining} width = "34px" alt="" style = {{width:"2em"}} />
                            <span style = {{color:"grey",fontSize:"1.5em"}}>&nbsp;{((crawling_data.remaining/crawling_data.total)*100).toFixed(0)}%</span>
                          </div>
                        </span>



                  </div>
              </div>
            </div>


          </div>


          <div className="main">
              <Dashboard allWebsitesData={allWebsitesData}/>
              <Controls allWebsites = {allWebsites}/>

          </div>

        </div>)
}

export default Analytics;
