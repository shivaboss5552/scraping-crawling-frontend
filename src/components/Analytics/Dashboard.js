import React,{useState,useRef} from 'react';
import SearchIcon from '../../assets/Analytics/search.svg';
import LeftIcon from '../../assets/Analytics/left.svg';
import RightIcon from '../../assets/Analytics/right.svg';
import CloseIcon from '../../assets/Analytics/close.svg';
import DateIcon from '../../assets/Analytics/date.svg';
import PulseLoader from '../../assets/Analytics/pulse.gif';

import Success from '../../assets/Analytics/success.svg';
import Failure from '../../assets/Analytics/fail.svg';
import Remaining from '../../assets/Analytics/remaining.svg';
import Scraper from '../../assets/Sidebar/scraper.png';
import Crawler from '../../assets/Sidebar/crawler.svg';

import Button from '@material-ui/core/Button';
import { Input } from '@material-ui/core';
import {Line,Bar} from 'react-chartjs-2';
import axios from 'axios';
import './Dashboard.css';

import name from '../domain.js';





function Dashboard(props){

  const initialDate = useRef('');
  const finalDate = useRef('');

  const [Caraousel,setCaraousel] = useState({
    lowerLimit:0,
    upperLimit:2,
    searchString:"",
    current:null,
    loading:false
  });

  const [query,setquery] = useState("");
  const {upperLimit,lowerLimit,searchString,current,loading} = Caraousel;

  const originalWebsiteData = props.allWebsitesData;
  const allWebsitesData = originalWebsiteData.filter(i => i.name.includes(searchString))


  const moveRight = () => {
    if (upperLimit < allWebsitesData.length-1){
      setCaraousel({...Caraousel,upperLimit:upperLimit+1,lowerLimit:lowerLimit+1})
    }
  }

  const moveLeft = () => {
    console.log("clicked",lowerLimit)
    if (lowerLimit > 0){
      setCaraousel({...Caraousel,upperLimit:upperLimit-1,lowerLimit:lowerLimit-1})
    }
  }

  async function submitDate(){
    console.log("clicked",initialDate.current.getElementsByTagName('input')[0].value,)

    let date1 = initialDate.current.getElementsByTagName('input')[0].value;
    let date2 = finalDate.current.getElementsByTagName('input')[0].value;

    const durationData = await axios.post(`http://${name}/api/duration/`,
      {initialDate:date1,finalDate:date2,websiteName:current.name}).then(res => res.data)

    console.log(durationData)
    setCaraousel({...Caraousel,current:{...current,duration:durationData}})
  }

  async function getDetails(event,websiteName){

      setCaraousel({...Caraousel,loading:true})

      const monthlyData = await axios.get(`http://${name}/api/timeline/${websiteName}`).then(res => res.data.data)

      const processedData = originalWebsiteData.filter(item => item.name === websiteName)[0]

      const websiteDetails = {name:websiteName,type:processedData.type,scraping_data:processedData.scraping_data,
                              crawling_data:processedData.crawling_data,monthlyData:monthlyData,duration:null}

      console.log(websiteDetails,"Website Details!")

      console.log({...Caraousel,loading:false,current:websiteDetails},"clicked")
      setCaraousel({...Caraousel,loading:false,current:websiteDetails})
  }






  return <div className="websites">
            <span className="websites-heading">Website Analytics</span>
            {loading ? <div id="pulse" >
                          <img  src={PulseLoader} width="128px" alt=""/>
                        </div>:


            (current === null  ? <div className="websites-container">

              <div className="websites-search">
                  <Input name = "searchField" value = {query} onChange ={(e) => setquery(e.target.value)} placeholder="Search For a Website..."/>
                  <img src={SearchIcon} onClick = {() => setCaraousel({...Caraousel,searchString:query,lowerLimit:0,upperLimit:2}) } width="32px" alt=""/>
                  {searchString !== "" &&   <img src={CloseIcon} onClick={() =>{
                        setquery("");
                       setCaraousel({...Caraousel,searchString:""})}} alt=""/>}

              </div>

              <div className="websites-caraousel">
                  <div className={`left-arrow ${0 !== lowerLimit && 'activated'}`} onClick = {moveLeft} >
                    <img src={LeftIcon}   width="64px" alt=""/>
                  </div>

                {allWebsitesData.map((item,index) => {
                    console.log(item)
                  if (index >=  lowerLimit && index <= upperLimit && item.name.includes(searchString))
                  {  console.log(item,"passed")
                      return <div className="website-grid">
                        <div className={`website-grid-hero ${item.type}-thumbnail`}></div>
                        <span>{item.name}</span>
                        <Button variant="contained" onClick = {function(e) { getDetails(e,item.name); } } color="primary">Details</Button>
                      </div>
                    }
                })}


                  <div className={`right-arrow ${allWebsitesData.length-1 !== upperLimit && 'activated'}`} onClick = {moveRight}>
                    <img src={RightIcon} width="64px" alt=""/>
                  </div>
              </div>

              <div className="websites-marker">
                {lowerLimit+1} - {upperLimit+1 > allWebsitesData.length ? allWebsitesData.length : upperLimit + 1} of { allWebsitesData.length}
              </div>
            </div> :
             <div className="websites-details-container">

                <div className="website-graph-container">

                  <div className="custom-search">
                    <div className="dateGroup" >
                      <img src={DateIcon} alt=""/>
                      <Input className = "searchField" name = "searchField"  ref={initialDate} placeholder="Starting Date (dd/mm/yyyy)"/>
                    </div>

                    <div className="dateGroup">
                      <img src={DateIcon} alt=""/>
                      <Input className = "searchField" name = "searchField" ref={finalDate} placeholder="Ending Date (dd/mm/yyyy)"/>
                    </div>

                    <img src={SearchIcon} className="hoverScale" width="32px" onClick = {submitDate} alt=""/>

                    {current.duration !== null && <img src={CloseIcon} className="hoverScale" onClick = {() => setCaraousel({...Caraousel,current:{...current,duration:null}})} alt=""/>}

                  </div>

                  <div className="website-timeline-graph">
                    {current.duration === null ?
                      <Line options={{ maintainAspectRatio: false }} data={{
                        labels: current.monthlyData.map(item => Object.keys(item)[0]).reverse(),
                        datasets: [
                          {
                            label: 'Scraping',
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: '#70ae6e',
                            borderColor: '#70ae6e',
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderColor: 'rgba(18, 102, 79,0.6)',
                            pointBackgroundColor: 'rgba(18, 102, 79,0.6)',
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                            pointHoverBorderColor: 'rgba(220,220,220,1)',
                            pointHoverBorderWidth: 2,
                            pointRadius: 3,
                            pointHitRadius: 10,
                            data: current.monthlyData.map(item => Object.values(item)[0].scraping).reverse()
                          },{
                            label: 'Crawling',
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: '#FF0000',
                            borderColor: '#FF0000',
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderColor: '#FF0000',
                            pointBackgroundColor: '#FF0000',
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: '#FF0000',
                            pointHoverBorderColor: '#FF0000',
                            pointHoverBorderWidth: 2,
                            pointRadius: 3,
                            pointHitRadius: 10,
                            data: current.monthlyData.map(item => Object.values(item)[0].crawling).reverse()
                          }
                        ]
                      }} /> :  <Bar
          data={{
  labels: ['Scraping', 'Crawling'],
  datasets: [
    {
      label: 'Duration Data',
      backgroundColor: 'rgba(255,99,132,0.2)',
      borderColor: 'rgba(255,99,132,1)',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(255,99,132,0.4)',
      hoverBorderColor: 'rgba(255,99,132,1)',
      data: [current.duration.scraping_data,current.duration.crawling_data]
    }
  ]
}}

          options={{
            maintainAspectRatio: false
          }}
        /> }
                  </div>

                </div>

                <div className="website-info-container">

                      <div className="website-info-name partition">
                        <span className="primary">Website Name</span>
                        <span className="secondary">{current.name} ({current.type})</span>
                      </div>

                      <div className="website-info-scraped-total partition">
                        <span className="primary">Total Scraped</span>
                        <span className="secondary">{current.scraping_data.total}</span>
                      </div>

                      <div className="website-info-scraped-last partition">
                        <span className="primary">Last Scraped</span>
                        <span className="secondary">{current.scraping_data.last}</span>
                      </div>

                      <div className="website-info-crawl-total partition">
                        <span className="primary">Total Crawled</span>
                        <span className="secondary">{current.crawling_data.total}</span>
                      </div>

                      <div className="website-info-crawl-last partition">
                        <span className="primary">Last Crawled</span>
                        <span className="secondary">{current.crawling_data.last}</span>
                      </div>

                      <div className="website-info-scraped-percentage iconset">
                        <div className="primaryIcon">
                          <img src={Scraper} width="20px" alt=""/>
                        </div>
                        <div className="secondaryIcon">
                            <div>
                              <img src={Success} width="20px" alt=""/>
                              <span>{((current.scraping_data.passed/current.scraping_data.total)*100).toFixed(1)}%</span>
                            </div>
                            <div>
                              <img src={Failure} width="20px" alt=""/>
                              <span>{((current.scraping_data.failed/current.scraping_data.total)*100).toFixed(1)}%</span>
                            </div>

                            <div>
                              <img src={Remaining} width="20px" alt=""/>
                              <span>{((current.scraping_data.remaining/current.scraping_data.total)*100).toFixed(1)}%</span>
                            </div>
                        </div>


                      </div>



                      <div className="website-info-crawl-percentage iconset">
                        <div className="primaryIcon">
                          <img src={Crawler} width="20px" alt=""/>
                        </div>
                        <div className="secondaryIcon">
                            <div>
                              <img src={Success} width="20px" alt=""/>
                              <span>{((current.crawling_data.passed/current.crawling_data.total)*100).toFixed(1)}%</span>
                            </div>
                            <div>
                              <img src={Failure} width="20px" alt=""/>
                              <span>{((current.crawling_data.failed/current.crawling_data.total)*100).toFixed(1)}%</span>
                            </div>

                            <div>
                              <img src={Remaining} width="20px" alt=""/>
                              <span>{((current.crawling_data.remaining/current.crawling_data.total)*100).toFixed(1)}%</span>
                            </div>
                        </div>
                      </div>

                      <div className="website-info-controls">
                        <div><Button onClick = {()=> {
                          console.log("clicked Back!")
                          setCaraousel({...Caraousel,current:null})
                        }} variant="contained"color="default">Back</Button></div>
                        <div><Button variant="contained"color="secondary">Reset</Button></div>
                      </div>


                </div>

              </div>





           )    }


        </div>
}

export default Dashboard;
