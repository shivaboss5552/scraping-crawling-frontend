import React from 'react';
import Profile from '../../assets/Header/profile.svg';
import './Header.css';



function Header(){

  return <div class="header">
            <span class="main-title"> <span id="partial">Scraping/Crawling</span>  Dashboard</span>
            <div class="profile">
                <span>Zeus</span>
                <img src={Profile} width = "48px" alt=""/>
            </div>
        </div>
}

export default Header;
