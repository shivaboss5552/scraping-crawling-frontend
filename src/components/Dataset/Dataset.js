import React,{useEffect,useState} from 'react'
import "./Dataset.css"



import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import name from "../domain.js";
import Loader from "../Loader/Loader";



const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});



function Dataset() {

    const [data,setData] = useState([]);
    const [toggleChoice,setToggleChoice] = useState(0);
    const [loading,setLoading] = useState(false);

    useEffect(function test() {
      
      async function fetchData() {
        setLoading(true);
        const data = await axios
          .get(`http://${name}/api/analytics/`)
          .then((res) => res.data);
        //const allWebsites = await axios.get("http://localhost:8000/api/websites/").then(res => res.data.active)
        //console.log(allWebsites.active)
        //const data = JSON.parse(window.localStorage.getItem("api"))
        console.log(data);

        setData(data);
        setLoading(false);

        
      }


      fetchData();
    }, []);

    const classes = useStyles();
    return (
      <div className="dataset">
          {loading ? <Loader/>:
        <div className="dataset__container">
          <div className="dataset__header">
              <span className="dataset__title">Tabular Scraping/Crawling Data</span>
            <div className="toggleSwitch">
              <label for="scraping" onClick={(e) => setToggleChoice(0)}>
                <input
                  type="radio"
                  checked={toggleChoice === 0}
                  name="dataset"
                  id="scraping"
                />
                <div className="actualRadio" id="leftSwitch">
                  Scraping
                </div>
              </label>
              <label for="crawling" onClick={(e) => setToggleChoice(1)}>
                <input
                  type="radio"
                  checked={toggleChoice === 1}
                  name="dataset"
                  id="crawling"
                />
                <div className="actualRadio" id="rightSwitch">
                  Crawling
                </div>
              </label>
            </div>
          </div>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow style={{ background: "#34386b" }}>
                  <TableCell className="dataset__cell">ID</TableCell>
                  <TableCell className="dataset__cell" align="right">
                    Website Name
                  </TableCell>
                  <TableCell className="dataset__cell" align="right">
                    Type
                  </TableCell>
                  <TableCell className="dataset__cell" align="right">
                    Total
                  </TableCell>
                  <TableCell className="dataset__cell" align="right">
                    Remaining
                  </TableCell>
                  <TableCell className="dataset__cell" align="right">
                    Passed
                  </TableCell>
                  <TableCell className="dataset__cell" align="right">
                    Failed
                  </TableCell>
                  <TableCell className="dataset__cell" align="right">
                    Last Activity
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((row, index) => {
                  const main =
                    toggleChoice === 0 ? row.scraping_data : row.crawling_data;

                  return (
                    <TableRow key={row.name}>
                      <TableCell
                        style={{ fontWeight: 600 }}
                        component="th"
                        scope="row">
                        {index + 1}
                      </TableCell>
                      <TableCell align="right">{row.name}</TableCell>
                      <TableCell align="right">{row.type}</TableCell>
                      <TableCell align="right">{main.total}</TableCell>
                      <TableCell align="right">{main.remaining}</TableCell>
                      <TableCell style={{ color: "green" }} align="right">
                        {main.passed}
                      </TableCell>
                      <TableCell style={{ color: "red" }} align="right">
                        {main.failed}
                      </TableCell>
                      <TableCell align="right">{main.last}</TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </div>}
      </div>
    );
}

export default Dataset;
