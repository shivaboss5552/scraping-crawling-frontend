import React,{Fragment,useEffect,useState} from 'react';
import ATGLogo from '../../assets/Sidebar/side_logo.png';
import Analytics from '../../assets/Sidebar/analysis.svg';
import Crawler from '../../assets/Sidebar/crawler.svg';
import TestBoard from '../../assets/Sidebar/testboard.svg';
import Documentation from '../../assets/Sidebar/documentation.svg';
import MenuIcon from '../../assets/Sidebar/menu.svg';
import './Sidebar.css'
import {Link} from "react-router-dom";


let style = {transform:"translatex(0%)"}

function Sidebar(){

  const [active,setActive] = useState(0);
  const [slide,setslide] = useState(true);
  const [show,setshow] = useState(false);

  if (slide){
    style = {...style,transform:"translatex(0%)"}
  }
  else{
    style = {...style,transform:"translatex(-100%)"}
  }

  window.onresize = doALoadOfStuff;

    function doALoadOfStuff() {

    if(window.innerWidth < 1170){
      setslide(false);
      setshow(true);
    }
    else{
      setslide(true);
      setshow(false);
    }
      }

  useEffect(function Setter(){

    const path = window.location.pathname.slice(1,);

    const directory = ['','tools','testboard','logs','help'].indexOf(path)
    console.log(path)

    if(window.innerWidth < 1170){
      setslide(false);
      setshow(true);
    }
    else{
      setslide(true);
      setshow(false);
    }

    setActive(directory !== -1 ? directory : 0)
  },[])



  return <Fragment>
      <img id="hamburger"  onClick={() => setslide(true)} src={MenuIcon} width="48px" alt=""/>
      <div className="sidebar" style={style} >
        <div className="sidebar-logo">
          <img id="atgLogo" src={ATGLogo} alt="logo" width="200px"/>
          {show && <img  onClick={() => setslide(false)} src={MenuIcon} width="42px" alt=""/>}
          {/* <img id="slide-menu"src={Drawer} width="48px" alt="menu"/>*/}
        </div>

        <ul className="sidebar-list">
          <li > <Link onClick = {(e)=>setActive(0)} className={0 === active ? 'active-tab' : ''} to="/"><img src={Analytics} width="64px" alt="analysis"/> <span>Statistics</span></Link></li>
          <li> <Link onClick = {(e)=>setActive(1)} className={1 === active ? 'active-tab' : ''} to="/tools" ><img src={Crawler} width="64px" alt="analysis"/> <span>Crawling</span></Link></li>
          <li> <Link onClick = {(e)=>setActive(2)} className={2 === active ? 'active-tab' : ''} to="/testboard"><img src={TestBoard} width="64px" alt="analysis"/> <span>TestBoard</span></Link></li>
          <li> <Link onClick = {(e)=>setActive(3)} className={3 === active ? 'active-tab' : ''} to="/dataset"><img src={Documentation} id = "last" width="64px" alt="analysis"/> <span>Dataset</span></Link></li>
          {/*<li  > <Link onClick = {(e)=>setActive(4)} className={4 === active ? 'active-tab' : ''} to="/help"><img src={Scraper} width="64px" alt="analysis"/> <span>Document</span></Link></li>*/}
        </ul>
      </div>
  </Fragment>
}

export default Sidebar;
