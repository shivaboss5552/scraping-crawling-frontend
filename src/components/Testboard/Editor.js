import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import './Editor.css';
import CloseIcon from '../../assets/Testboard/close.svg';
import GearIcon from '../../assets/Testboard/gear.svg';
import BackIcon from '../../assets/Testboard/back.svg';
import LoadIcon from '../../assets/Testboard/loader.gif';
import SaveIcon from '../../assets/Testboard/save.svg';
import Alert from '@material-ui/lab/Alert';

import React,{useState,useEffect,useRef,Fragment} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {Button} from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import './Fetcher.css';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import name from '../domain.js';


const useStylesAlert = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

const useStylesDropdown = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
}));

const useStylesSelect = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={false}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function Editor(props){

  const selectRef = useRef('');
  

  const classesSelect = useStylesSelect();
  const [age, setAge] = React.useState('');
  const classesDropdown = useStylesDropdown();
  const AlertClasses = useStylesAlert()
  const [alert,setalert] = useState({show:false,message:"",type:""});

  const [mainJSON,setmainJSON] = useState({all:[],current:null})
  const [processedJSON,setprocessedJSON] = useState(null);
  const [loading,setloading] = useState(false);
  const [testURL,settestURL] = useState('');

  const {current} = mainJSON;

  const handleChanges = (event) => {
    setAge(event.target.value);
  };

  useEffect(function Loader(){

    async function getWebsites(){
      const allWebsites = await axios.get(`http://${name}/api/websites/`).then(res => res.data.active)
       setmainJSON({all:allWebsites,current:null})
    }
    getWebsites()

  },[])

  async function getDetails(e){
    const websiteName = selectRef.current.getElementsByTagName('input')[0].value
    if(websiteName){
    const websiteDetails = await axios.get(`http://${name}/api/websites/${websiteName}`).then(res => res.data)
    const processedDetails = {...websiteDetails,map_json:JSON.parse(websiteDetails.map_json)}
    console.log(processedDetails)
    setmainJSON({...mainJSON,current:processedDetails})}

  }

  const updateJSON = (event,is_map_json) => {


    if (is_map_json){
      setmainJSON({...mainJSON,current:{...current,map_json:{...current.map_json,[event.target.name]:event.target.value}}})
    }
    else{
      setmainJSON({...mainJSON,current:{...current,[event.target.name]:event.target.value}})
    }

  }

  async function testboard(){
    setloading(true);
    const testingURL = testURL
    if (testURL){
    const processedData = {map_json:JSON.stringify(current.map_json),url:testingURL,website:current.website_name}

    const response = await axios.post(`http://${name}/api/fetch/`,processedData).then(res => res.data).catch(err => err.response)

    console.log(response)
    setprocessedJSON(response)
    setValue(1)}
    setloading(false)

  }

  async function updateWebsites(){
    const processedData = {website_name:current.website_name,url:current.url,regex:current.regex,feature:current.feature,
                          map_json:JSON.stringify(current.map_json),crawling_or_paused:current.crawling_or_paused,group_id_fk:(current.group_id_fk === null ? '' : current.group_id_fk),
                          exception_group_id_fk:(current.exception_group_id_fk === null ? '' : current.exception_group_id_fk)}
    const response = await axios.put(`http://${name}/api/websites/`,processedData).then(res => res.data).catch(err => err.response.data)

    if (Object.keys(response).includes("website_name")){
      setalert({show:true,message:"Website Updated Successfully!",type:"success"});
      setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);
      setmainJSON({...mainJSON,current:null});
    }
    else if (Object.keys(response).includes("error")){
      setalert({show:true,message:response.error,type:"error"});
      setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);
    }

    else{
      setalert({show:true,message:"Something went wrong!",type:"error"});
      setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);
    }
    console.log(response)
  }

  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
          <Tab label="Edit Websites" {...a11yProps(0)} />
          <Tab label="Results" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
      <div className="fetcher-container">

              {current === null && <Button onClick ={() => props.setSection(0)} id="backButton" variant="contained" color="default"> <img src={BackIcon} width ="32px" alt=""/>&nbsp;&nbsp;<span>Go Back!</span></Button>}
              <div className="fetch-controller">


                    <FormControl id="main-select" className={classesSelect.formControl}>
                        <InputLabel id="demo-simple-select-label">Select A Website</InputLabel>
                        <Select
                          ref= {selectRef}
                          labelId="demo-simple-select-label"
                          id="demo-simple-select"
                          value={age}
                          onChange={handleChanges}
                        >
                          {mainJSON.all.map(item => <MenuItem value={item}>{item}</MenuItem>  )}

                        </Select>
                    </FormControl>



                    {current === null ? <Button  variant="contained" color="primary" onClick={getDetails} ><img src={GearIcon} alt=""/>&nbsp;&nbsp;<span>Fetch!</span> </Button> : <Button  variant="contained" color="default" onClick={()=>{
                      setmainJSON({...mainJSON,current:null})
                      setprocessedJSON(null)
                      settestURL('')}} ><img src={CloseIcon} alt=""/>&nbsp;&nbsp;<span>Clear!</span></Button>}

              </div>

              {current !== null &&
                <Fragment>

                    <TextField id="standard-basic" name="url" value={current.url} onChange={(e) => updateJSON(e,false)} label="Website's URL" />
                    <TextField id="standard-basic" name="regex" value={current.regex} onChange={(e) => updateJSON(e,false)} label="Website's Regex" />
                    <TextField id="standard-basic" name="group_id_fk" value={current.group_id_fk} onChange={(e) => updateJSON(e,false)} label="Website's Included Groups" />
                    <TextField id="standard-basic" name="exception_group_id_fk" value={current.exception_group_id_fk} onChange={(e) => updateJSON(e,false)} label="Website's Excluded Groups" />
                    <div className="jsonHeader">Test Map JSON fields on Test URLs here</div>
                    <div className="execute">
                      <TextField id="standard-basic" name="test-url" onChange={(e) => settestURL(e.target.value)} value ={testURL}  label="Enter the Test Url for this Website!" />
                      <Button  variant="contained" color="secondary" onClick={testboard} ><img src={GearIcon} alt=""/>&nbsp;&nbsp;<span>Test JSON</span></Button>
                      {loading && <img src={LoadIcon} style={{width:"32px"}} alt=""/>}
                    </div>
                    {Object.keys(current.map_json).map( key => <TextField id="standard-basic" name={key} value={current.map_json[key]} onChange={(e) => updateJSON(e,true)} label={key} />)}

                    <Button  variant="contained" color="primary" onClick={updateWebsites} ><img src={SaveIcon} alt=""/>&nbsp;&nbsp;<span>Update Website!</span></Button>

                </Fragment>}

                {alert.show &&
                <div className={AlertClasses.root}>
                  <Alert className="flash-box"  severity={alert.type}>{alert.message}</Alert>
                </div>}

            </div>
      </TabPanel>
      <TabPanel value={value} index={1}>

      {processedJSON && <div className={classesDropdown.root}>
        {/*{expand ? <Button  variant="contained" color="primary" onClick={() => setexpand(false)} > Collapse All! </Button> : <Button  variant="contained" color="primary" onClick={() => setexpand(true)} > Expand All! </Button>}*/}

        {Object.keys(processedJSON).filter(item => processedJSON[item] !== "" ).map(key => (
          <Fragment>

            <ExpansionPanel defaultExpanded={true}>
              <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}><h3>{key}</h3></Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Typography>
                  <span className={processedJSON[key].toLowerCase().includes('error') || processedJSON[key].toLowerCase().includes('warning') ? 'errorText' : ''}>{processedJSON[key]}</span>
                  {key==="profile_image" && <img src={processedJSON[key]} alt=""/>}
                </Typography>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </Fragment>))}
          </div> }
      </TabPanel>
    </div>
  );
}
