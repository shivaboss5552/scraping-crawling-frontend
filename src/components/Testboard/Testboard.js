import React,{useState} from 'react';
import './Testboard.css';
import AddIcon from '../../assets/Testboard/add.svg';
import EditIcon from '../../assets/Testboard/edit.svg';
import Adder from './Adder.js';
import Editor from './Editor.js';


function Testboard(){

  const [Section,setSection] = useState(0);


  const choiceRender = (section) => {
    switch(section) {
        case 0:
          return <div className="testboard-box">
              <div className="testboard-heading">Add & Test Websites</div>
               <div className="testboard-choices">
                <span className = "banner"> OUR TOOLS </span>
                <div>
                  <div className="testboard-cards" onClick={()=>setSection(1)}>
                    <img src={AddIcon} width="128px" alt=""/>
                    <span>Initiate Scraping/Crawling on a Website by adding it from here!</span>
                  </div>
                  <div className="testboard-cards" onClick={()=>setSection(2)}>
                    <img src={EditIcon} width="110px" alt=""/>
                    <span>Add Map JSON to new websites or use this tool to debug existing Websites! </span>
                  </div>
                </div>
              </div>
          </div>

          
        case 1:
          return <div className="testboard-box">
                    <div className="testboard-heading">Add & Test Websites</div>
                    <Adder setSection={setSection} />
                </div>
          
        case 2:
          return <div className="testboard-box">
                  <Editor setSection={setSection} />
                </div>
          
        default:
          return <h1>adwad</h1>
      }
  }

  return     <div className="testboard-container">


                        {choiceRender(Section)}

                  </div>



  }

export default Testboard;
