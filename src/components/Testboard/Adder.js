import React,{useState} from 'react';
import './Adder.css';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import {Button} from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Alert from '@material-ui/lab/Alert';
import BackIcon from '../../assets/Testboard/back.svg';
import axios from 'axios';




const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const useStylesAlert = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

function Adder(props){
  
  const [form,setform] = useState({name:"",url:"",regex:"",type:"",include_groups:"",exclude_groups:""});
  const [alert,setalert] = useState({show:false,message:"",type:""});

  const {name,url,regex,type,include_groups,exclude_groups} = form;
  const classes = useStyles();
  const AlertClasses = useStylesAlert()

  const handleChange = (event) => {
    setform({...form,[event.target.name]:event.target.value})
  };

  async function formProcess (){
    console.log(form)
    if (!form.name || !form.url || !form.regex || !form.type){
      setalert({show:true,message:"Please fill all the mandatory fields!",type:"error"})
      setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);
    }
    else{
      const processed_data = {website_name:form.name,url:form.url,regex:form.regex,feature:form.type,
                        group_id_fk:form.include_groups,exception_group_id_fk:form.exclude_groups}
      console.log(processed_data,"this is form data")

      const response =  await axios.post(`http://${name}/api/websites/`,processed_data).then(res => res.data).catch(err => err.response.data)
      console.log(response,"this is response")

      if (Object.keys(response).includes("website_name")){
        setalert({show:true,message:"Website Added Successfully!",type:"success"});
        setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);
        setform({name:"",url:"",regex:"",type:"",include_groups:"",exclude_groups:""});
      }
      else if (Object.keys(response).includes("error")){
        setalert({show:true,message:response.error,type:"error"});
        setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);
      }

      else{
        setalert({show:true,message:"Something went wrong!",type:"error"});
        setTimeout(function(){ setalert({show:false,message:"",type:""}) }, 3000);
      }

    }

  }

  return <div className="addwebsites-container">
            <Button onClick ={() => props.setSection(0)} id="backButton" variant="contained" color="default"> <img src={BackIcon} width ="32px" alt=""/>&nbsp;&nbsp;<span>Go Back!</span></Button>
          {alert.show &&
          <div className={AlertClasses.root} id="flash">
            <Alert  severity={alert.type}>{alert.message}</Alert>
          </div>}
          <h1 className ="emphasis">Add a Website</h1>
          <TextField id="standard-basic" name="name" value={name} onChange={handleChange} label="Enter Website's Name" />
          <TextField id="standard-basic" name="url" value={url} onChange={handleChange} label="Enter Website's Base URL" />
          <TextField id="standard-basic" name="regex" value={regex} onChange={handleChange} label="Enter Website's URL matching regex" />
          <FormControl className={classes.formControl}>
              <InputLabel id="demo-simple-select-label">Select Website Type</InputLabel>
              <Select
                name="type" value={type}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                onChange={handleChange}
              >
                <MenuItem value="job">Jobs</MenuItem>
                <MenuItem value="event">Events</MenuItem>
                <MenuItem value="education">Education</MenuItem>
              </Select>
         </FormControl>
         <TextField id="standard-basic" name="include_groups" value={include_groups} onChange={handleChange} label="Enter groups to Include (comma separated)" />
         <TextField id="standard-basic" name="exclude_groups" value={exclude_groups} onChange={handleChange} label="Enter groups to Exclude (comma separated)" />
         <Button onClick={formProcess} variant="contained" color="primary">
           Add!
         </Button>
        </div>
}

export default Adder;
