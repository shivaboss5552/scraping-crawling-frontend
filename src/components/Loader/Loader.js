import React from 'react';
import LoaderIcon from './Loader.gif';


function Loader(){

  return <div id="loader">
          <img src={LoaderIcon} width="100px" alt=""/>
        </div>
}

export default Loader;
